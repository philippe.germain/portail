# Récolte de QCM

* [qcm_nsi_1re.csv](qcm_nsi_1re.csv) - données brutes issues du questionnaire en ligne [https://framaforms.org/qcm-nsi-1re-1561056860](https://framaforms.org/qcm-nsi-1re-1561056860)

* [qcm.md](qcm.md) - mise en forme Markdown du précédent

* [csv-qcm.py](csv-qcm.py) - outils pour la génération de Markdown à partir du fichier CSV


