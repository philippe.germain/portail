Codage de Huffman
=================

Évaluation - rendre votre travail
---------------------------------

Le travail réalisé dans le cadre de ce TP sera pris en compte dans l'évaluation.

Ce travail sera rendu via un dépôt GitLab ad hoc. Voyez les instructions à

* [gitlab-fil.univ-lille.fr/diu-eil-lil/huffman](https://gitlab-fil.univ-lille.fr/diu-eil-lil/huffman)

Sujet de travaux pratiques
--------------------------

Le sujet rédigé dans un notebook jupyter est accessible

* dans le fichier [tp_codage_huffman.ipynb](tp_codage_huffman.ipynb) via le serveur `jupyter.fil.univ-lille.fr` à [frama.link/diu-ipynb-huffman](https://s.42l.fr/diu-ipynb-huffman)
  <!-- https://jupyter.fil.univ-lille.fr/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab-fil.univ-lille.fr%2Fdiu-eil-lil%2Fipynb&urlpath=tree%2Fipynb%2Fhuffman%2Ftp_codage_huffman.ipynb -->
* dans un fichier source Python généré depuis ce notebook [tp_codage_huffman.py](tp_codage_huffman.py)

Vous travaillez à votre préférence dans un notebook ou Thonny.

Les fichiers annexes suivants sont également fournis :

* [binary_tree.py](../arbres/binary_tree.py)
* [binary_IO.py](binary_IO.py)
* [cigale-UTF-8.txt](cigale-UTF-8.txt)
* [exple_huffman_tree.png](exple_huffman_tree.png)

Informatique sans ordinateur
----------------------------

Les marmottes au sommeil léger

> Un groupe de marmottes doit creuser un nouveau terrier pour l’hiver, en minimisant le bruit dû aux déplacements desdites marmottes pendant l’hibernation. Une façon surprenante de parler de compression de texte, accessible dès la fin de primaire.

* activité d'informatique sans ordinateur proposée par Marie Duflot
  [members.loria.fr/MDuflot/files/med/marmottes.html](https://members.loria.fr/MDuflot/files/med/marmottes.html)
