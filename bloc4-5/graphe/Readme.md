Graphes
=======

(pour mémoire, ressources relatives aux graphes utilisées avec la 1re promo
 [Readme-1820.md](Readme-1820.md))

Travaux pratiques - découverte des graphes

* notebook jupyter [graphe.ipynb](graphe.ipynb) 
* également en ligne sur le serveur `jupyter.fil.univ-lille.fr` via 
  [frama.link/diu-ipynb-graphe](https://s.42l.fr/diu-ipynb-graphe) 

<!-- 
https://jupyter.fil.univ-lille.fr/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab-fil.univ-lille.fr%2Fdiu-eil-lil%2Fipynb&urlpath=tree%2Fipynb%2Fgraphes%2Fgraphe.ipynb
--> 

Cours - 6 problèmes 

1. présentation des problèmes
   - notebook jupyter [graphe-pres.ipynb](graphe-pres.ipynb) 
   - également en ligne par le serveur `jupyter.fil.univ-lille.fr` via
     [frama.link/diu-ipynb-graphe-pres](https://s.42l.fr/diu-ipynb-graphe-pres) 
2. modélisation et résolution des problèmes
   - notebook jupyter [graphe-code.ipynb](graphe-code.ipynb) 
   - également en ligne par le serveur `jupyter.fil.univ-lille.fr` via
     [frama.link/diu-ipynb-graphe-code](https://s.42l.fr/diu-ipynb-graphe-code)

<!-- 
https://jupyter.fil.univ-lille.fr/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab-fil.univ-lille.fr%2Fdiu-eil-lil%2Fipynb&urlpath=tree%2Fipynb%2Fgraphes%2Fgraphe-pres.ipynb

https://jupyter.fil.univ-lille.fr/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab-fil.univ-lille.fr%2Fdiu-eil-lil%2Fipynb&urlpath=tree%2Fipynb%2Fgraphes%2Fgraphe-code.ipynb
--> 
