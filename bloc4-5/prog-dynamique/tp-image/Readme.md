# Programmation Dynamique

## Problème 1

Le problème :
- On a un triangle de $`n`$ lignes de nombres entiers.
- On part du sommet.
- À chaque étape, on choisit dans la ligne du dessous un des
  **deux nombres** adjacents.
- On s'arrête quand on est sur la dernière ligne.
- On cherche à **maximiser la somme totale** des nombres choisis.

![triangle](./triangle.jpg)

Il s'agit donc bien d'un problème d'*optimisation combinatoire*. 
Pour plus de renseignements, voir [la page de wikipédia](https://fr.wikipedia.org/wiki/Optimisation_combinatoire).

Une solution simple consiste à énumérer **tous les chemins** pour déterminer celui de somme maximale. En pratique,
ceci est impossible, car il y a :

```math
2^{n-1}
```

chemins différents.

### En python

**Donnée :** une liste $`n`$ lignes. Par exemple :


```python
tab = [ [5], [8, 10 ], [11, 3, 4], [6, 10, 7, 12 ]]
```

représente notre triangle.

### Analyse

Pour résoudre ce problème, nous pouvons considérer la notion de gain optimal. Le gain optimal d'un
point $`p`$ du triangle est la somme maximale que l'on peut réaliser en partant d'un point de **la
dernière ligne** jusqu'au point $`p`$.

Il s'agit d'un sous problème de notre problème d'optimisation.

**Sous-problème :** Calculer le gain optimal d'un point quelconque du triangle.

Nous allons exprimer le gain optimal d'un point $`p`$ en fonction des gains optimaux des deux points
de la ligne suivante adjacents à $`p`$.

On repère le point $`p`$ par son numéro de ligne $`l`$ ($`0\leqslant l < n`$) et de colonne $`r`$ ($`0
\leqslant r \leqslant l`$).

On note $`G(l, r)`$ le gain maximum du point $`p(l, r)`$.

Ce gain peut s'exprimer en fonction des gains des points de la ligne suivante :

```math
\begin{array}{rcl}
 G(l, r) & = & tab[l][r] + max(G(l+1, r), G(l+1, r+1))\,\forall 0\leq l\leq n-2, 0\leq r\leq l \\
 G(n-1, r) &  = & tab[n-1][r]
\end{array}
```

**À faire** Écrire une fonction recursive `solution_naive(t: list,l: int ,r: int) -> int` implantant l'algorithme ci-dessus.


```python
solution_naive([[1], [2,3]], 0, 0)
```

    4


**Complexité :** Notons $`C(l)`$ le nombre de lectures dans le tableau `tab` nécessaires au calcul
du gain d'un point de la ligne $`l`$.  Les nombres $`C(l)`$ vérifient les équations de récurrences :

```math
\begin{array}{rcl}
C(l) &=& 1+2C(l+1) \\
C(n-1) &=& 1
\end{array}
```

On en déduit que :

```math
\begin{array}{rcl}
C(n-2) &=& 1 + 2\\
C(n-3) &=& 1 + 2 + 4\\
&\ldots & \\
C(0) & = & 1 + 2 + \ldots + 2^{n-1}
\end{array}
```

On a donc $`C(0)=2^n-1`$, et la complexité est encore exponentielle ...

**Remarques :**
- Il ne peut y avoir plus d'appel différents aux fonctions que de couples $`(l,r)`$, soit
  $`\frac{n(n+1)}{2}`$ appels ;
- Donc on recalcule de nombreuses fois les mêmes valeurs ;
- Nous allons utiliser une table pour mémoriser les valeurs.


**À faire :** Écrire une fonction `solution_dynamique_memoization(tab, l, r)` prenant en paramètre
un tableau de nombres et renvoyant le poids de la solution optimale. Cette solution utilisera un
dictionnaire pour mémoriser les valeurs :

```python
def solution_dynamique_memoization(tab: list, l: int, r: int, mem=None) -> int:
    if mem is None:
        mem = {}
    if (l, r) in mem:
        return mem[(l, r)]
    else:
        ...
```

En utilisant une approche bottom-to-top, on peut se passer de la récursivité. 

**À faire** Écrire une fonction `solution_dynamique(tab)` qui crée le tableau des gains optimaux, le
remplit ligne par ligne en partant du bas du tableau, et renvoie la valeur du tableau des gains
situé à la première ligne, première colonne.

Dans les deux fonctions précédentes, la complexité en temps et en espace est proportionnelle à la
taille du tableau `gain`.

Donc

```math
C(n)=\Theta(n^2)
```

### Retrouver son chemin.

**À faire** En utilisant le tableau des gains optimaux, modifier votre fonction `solution_dynamique`
pour qu'elle renvoie la liste des nombres dont la somme est maximale.

**Indication :** Utiliser le tableau des gains optimaux et le tableau des nombres et opérer en
"remontant" (approche bottom-to-top).

## Problème 2 : Redimensionnement d'image

Il y a plusieurs méthodes pour redimensionner une image. Les plus connues sont de tronquer l'image
ou de la rétrécir, mais chacune de ces méthodes a des inconvénients (perte de l'information ou
déformation de l'image).

Supposons que l'on veuille diminuer la largeur d'une image (le cas de la hauteur est similaire); une
possibilité est de chercher à supprimer une "colonne" de pixels du milieu de l'image. Toutefois, la
suppression d'une colonne exactement verticale serait très visible sur l'image.

Dans ce TP, on cherche à déterminer une colonne de pixel possédant une énergie minimale, c'est à
dire dont la suppression va le moins impacter l'image.

Ces colonnes de pixels sont appelées **coutures**. En supprimant la couture d'énergie minimale, on
diminue la largeur de l'image d'un pixel. En répétant ce procédé, on peut réduire significativement
la largeur de l'image.

### Énergie d'une image

Dans la recherche des coutures d'énergie minimum, il est nécessaire de définir l'énergie de chaque
pixel.

Plusieurs solutions sont envisageables pour définir l'énergie. Ici, nous allons définir l'énergie
d'un pixel $`p`$ de coordonnées $`(x, y)`$ en calculant pour chaque composante Rouge, Verte, Bleue :
1. La différence entre la valeur de la composante du pixel situé à gauche de $`p`$ et la valeur de
   la composante du pixel situé à droite de $`p`$, élevée au carré ;
2. La différence entre la valeur de la composante du pixel situé au dessus de $`p`$ et la valeur de
   la composante du pixel situé en dessous de $`p`$, élevée au carré.
   
L'énergie de $`p`$ est alors la somme des sommes de ces nombres.

Ceci nous donne, pour le pixel $`p`$ situé aux coordonnées $`(x,y)`$ : 

```math
\begin{array}{rcl}
\Delta_x & = &(\Delta r_x)^2+(\Delta g_x)^2+(\Delta b_x)^2\\
\Delta_y & = &(\Delta r_y)^2+(\Delta g_y)^2+(\Delta b_y)^2\\
e(x,y) & = & \Delta_x + \Delta_y
\end{array}
```

où $`\Delta r_x`$ désigne, par exemple, la différence de la composante rouge ($`r`$) entre les
pixels situés aux coordonnées $`(x-1,y)`$ et $`(x+1, y)`$ et $`\Delta r_y`$ désigne la différence de
cette même composante pour les pixels situés aux coordonnées $`(x,y-1)`$ et $`(x, y+1)`$.

**Remarque :** Pour les pixels situés sur une bordure, par exemple les pixels situés à gauche de
l'image, on considère non plus les variations entre ses deux voisins, mais les variations entre **le
pixel** et son voisin.

## Utilisation de PIL

La manipulation des images peut être réalisée à l'aide de la bibliothèque PIL (Python Imaging
Library).

La méthode `open` de la classe image permet de charger d'une image, la méthode `convert` permet de
la convertir en RVB :

```python
from PIL import Image
import random
surf = Image.open("surfer.jpg").convert('RGB')
surf
```

![png](output_23_0.png)


L'attribut `size` permet d'obtenir les dimensions de l'image :

```python
surf.size
```

    (1920, 1079)


La récupération des composantes rvb, est possible grace à la méthode `getpixel` :

```python
surf.getpixel((100,200))
```

    (95, 169, 170)


## Mise en oeuvre

**À faire** Écrire une fonction `delta_energy(p1: tuple, p2: tuple) -> int` prenant en paramètre
deux pixels sous la forme de deux triplets RVB et renvoyant la somme des carrés des différences des
composantes RVB des deux pixels.


```python
delta_energy((1, 2, 3), (2, 3, 4))
```

    3


**À faire** Écrire une fonction `neighbors(coord: tuple, dim: tuple) -> tuple` prenant en paramètre
les coordonnées d'un pixel de l'image et les dimensions $`(w,h)`$ de l'image et renvoyant les
coordonnées des quatre voisins sous la forme d'un tuple contenant les voisins horizontaux, puis les
voisins verticaux. Votre fonction traitera comme expliqué plus haut les pixels situés sur les bords
de l'image.


```python
neighbors((4, 4), (10,10))
```

    ((3, 4), (5, 4), (4, 3), (4, 5))


**À faire** Écrire une fonction `image_energy(im: Image) -> list` prenant en paramètre une image `im` au format
PIL convertie en RVB et renvoyant une liste de listes contenant les énergies des pixels de l'image.

On peut visualiser les énergies des pixels en créant une image :

```python
en = image_energy(im)
h, w = len(en), len(en[0])
energy_map = Image.new("L", (w, h),255)
for y in range(h):
    for x in range(w):
        energy_map.putpixel((x,y),(en[y][x]//16,)) # on divise par 16 (empirique) car les valeurs sont tronquées à 255
energy_map
```

![energies des pixels](output_35_0.png)

### Utiliser la programmation dynamique pour trouver les coutures d'énergie minimale

La notion d'énergie ayant été définie, on peut s'attaquer à la recherche des coutures d'énergie
minimale. Nous allons traiter le cas des coutures verticales, mais les coutures horizontales peuvent
se traiter de la même façon (ou pas : il suffit de tourner l'image de 90°)

Commençons par définir la couture d'énergie minimale :
- une couture est une séquence de pixels adjacents contenant un seul pixel par ligne.
- La couture d'énergie minimale est celle qui minimise la somme des énergies des pixels de la
  couture.

Comme pour le problème de le somme maximum, le choix à chaque ligne du pixel adjacent d'énergie
minimal ne permet pas d'obtenir la couture minimale : l'algorithme glouton n'est pas adapté.

Nous allons ici utiliser la programmation dynamique et utiliser un tableau définissant pour chaque
pixel de coordonnées $`(x,y)`$ de l'image, l'énergie minimale $`M(x,y)`$ de la couture d'énergie
minimale partant du bord supérieur de l'image et conduisant à ce pixel.


 Pour les pixels situés sur le bord supérieur de l'image, le calcul est facile : il s'agit de
 l'énergie de chaque pixel :
 
 ```math
M(x, 0) = e(x,0)
```
 
 Pour le pixel situé aux coordonnées $`(x,y)`$, il suffit de comparer les énergies minimales
 atteignables aux trois pixels $`(x-1,y-1), (x, y-1), (x+1, y)`$. La valeur $`M(x,y)`$ est définie
 alors comme la somme de l'énergie du pixel et du minimum des énergies minimales des 3 pixels :
 
 ```math
M(x,y) = e(x,y) + min\left(M(x-1,y-1);M(x,y-1);M(x+1,y-1)\right)
```
 
 Pour les pixels situés sur les bords gauches ou droits, le minimum sera calculé en utilisant les
 valeurs des énergies minimales des deux pixels adjacents de la ligne du dessus.
 
 En itérant ce procédé ligne par ligne, on construit le tableau des énergies des coutures minimales.

**À faire** Écrire une fonction prenant en paramètre le tableau des énergies des pixels de l'image
et qui renvoie le tableau des énergie des coutures minimales tel que défini ci-dessus.


```python
s_en = seams_energy(en)
```


Pour déterminer la couture d'énergie minimale, il suffit de choisir les coordonnées $`(x_0,h-1)`$
d'un pixel de la dernière ligne de l'image vérifiant

```math
 M(x_0,h-1)=min_{0\leq x <w}M(x,h-1)
```

Puis de "remonter" la couture en utilisant l'énergie du pixel pour déterminer lequel des trois
pixels situés au dessus permet de réaliser ce minimum.

**À faire** Écrire une fonction `minimal_seam(min_energy, energy)` prenant en paramètre le tableau
des énergies minimales et le tableau des énergies des pixels et qui renvoie une couture d'énergie
minimale sous la forme d'une liste de coordonnées.


```python
seam = list(minimal_seam(s_en,en))
for p in seam:
    surf.putpixel(p,(255,0,0))
surf
```


![png](output_44_0.png)



**À faire** Finalement, écrire une fonction `redim_width(im,n)` prenant en paramètre une image `im`
de dimension $`(w,h)`$ et un nombre `n` et qui renvoie une nouvelle image ayant pour dimensions
$`(w-n, h)`$ en utilisant la méthode des coutures minimales.

Voici l'image de test et le résultat sur cette image en ayant supprimé 100 pixels en largeur :

![surfeur](output_48_0.png)

![surfeur -100 px](output_47_1.png)

