---
title: Bases de données relationnelles
subtitle: SQL 1
author: |
    | Maude Pupin
    | Univ. Lille 
date: |
    | juin 2020
    |
    | 
    | \protect CC-BY
lang: fr
---

<!-- à compiler par
pandoc -t beamer --slide-level 2 -V "aspectratio=1610" -s cours_sql1.md -o cours_sql1.pdf
--> 

## SQL et bases de données relationnelles

### Menu

* Base de données relationnelles
  * Modèle relationnel
  * Structure de la base de notre exemple
  * Construire une structure optimisée
* SQL
  * Description de SQL
  * Extraction des données d'une table
  * Fonctions de calcul sur les données extraites
  * Extraction des données de deux tables
  * Modification des données

# Bases de données relationnelles

## Modèle relationnel

### Relation = table à 2 dimensions  

* Groupe d'informations relatives à un sujet
* Colonnes = attributs
* Lignes = n-uplets = enregistrements
* Cases = entrées = valeurs   

\  

![données de la table ville](fig/table_ville.png "données de la table ville")

## Modèle relationnel

### Connexions entre relations

* Base de données = ensemble de sujets connexes répartis dans des tables
* Les liens sont stockés via les clés primaires et étrangères de 2 tables

### Clé primaire = Primary Key (PK)
* Identifie de manière unique une ligne
* Ne doit pas être `NULL` (vide)
* Peut être composée d'une ou plusieurs colonnes
* Ajout d'une colonne dédiée si besoin

### Clé étrangère = Foreign Key (FK)
* Référence une ou plusieurs colonnes d'une autre table (représentant
une clé primaire)
* Les colonnes référencées doivent pré-exister dans la table référencée

## Structure de la base de notre exemple

![structure de la base categories-socio-nord](fig/uml_cat-socio-nord.png "structure de la base categories-socio-nord")

## Structure de la base de notre exemple

### Table `ville`
* Contient des données générales sur les villes du Nord
* `code` = code officiel géographique INSEE pour les communes
  * chaque code est unique et propre à une ligne de la table `ville`
  * la colonne `code` est la clé primaire de la table `ville`

### Table `evolution`
* Contient des effectifs genrés de catégories socioprofessionnelles par ville du Nord
* Plusieurs effectifs sont donnés pour chaque ville
* Les villes sont référencées par leur code
  * un même code peut être présent dans plusieurs lignes de la table `evolution`
  * la colonne `code` est la clé étrangère de la table `evolution` qui référence
  la colonne `code` de la table `ville`

## Construire une structure optimisée

### Regrouper les données en tables
* Mettre dans une même table les données relatives à un même sujet
* Créer de nouvelles tables pour éviter la redondance des données
  * Limite les incohérences lors des mises à jour
  * Facilite la construction des requêtes et améliore la pertinence des résultats

### Établir les relations entre tables
* Définir les clés primaires
  * Uniques et non NULL
* Définir les clés étrangères
  * Référencent les clés primaires

### Définir des colonnes pertinentes
* Facilité d'interrogation des colonnes
* Données cohérentes au sein d'une colonne
* Ne pas conserver des données qui peuvent être calculées

## Construire une structure optimisée

### Cas des catégories socioprofessionnelles du Nord

![Fichier d'origine : CSV](fig/categorie_socio_csv.png "Fichier d'origine : CSV")

## Construire une structure pertinente

### Regrouper les données en tables
* Tables = données relatives à un même sujet
  * Données sur les villes et sur les effectifs des catégories

### Éviter la redondance des colonnes
* var = Tranche + categorie
* categorie = abréviation Catégorie socioprofessionnelle

### Éviter la redondance des valeurs
* Les colonnes `departement` et `region` ne contiennent qu'une seule valeur
* À conserver uniquement si volonté d'étendre à d'autres données

### Données cohérentes
* Les colonnes Sexe, Tranche, _etc._ contiennent aussi des totaux !
  * Les totaux peuvent être calculés à l'aide de fonctions et d'agrégats
* Décomposer `coordonnees` en deux REAL 


<!--
## Construire une structure pertinente

### Proposition d'une structure avec encore moins de redondance

![Structure moins redondante](fig/uml2_cat-socio-nord.png "Structure moins redondante")
-->

# SQL : Structured Query Language

## Description de SQL

### Langage informatique servant à exploiter des bases de données relationnelles

* Manipulation des données
  * Recherche de données : `SELECT`
  * Ajout de données : `INSERT`
  * Modification de données : `UPDATE`
  * Suppression de données : `DELETE`
  
\  

* Définition des données
  * Manipule les structures de données de la base
  * Création de tables et autres structures : `CREATE`
  * _etc._
   
\   
   
* Contrôle des données et des transactions
  * Gestion des autorisations d'accès aux données par les différents utilisateurs
  * Gestion de l'exécution de transactions
    * Transaction = suite d'opérations de modification de la base de données

## Description de SQL

### SGBDR = Système de Gestion de Bases de Données Relationnelle
* Logiciel permettant de manipuler le contenu des bases de données relationnelles
* Garantit la qualité, la pérennité et la confidentialité des informations
* Exemple : [SQLite](https://sqlite.org/) est un SGBDR dont le code
source est dans le domaine public ![logo SQLite](fig/SQLite370.svg.png)

### C'est un langage déclaratif
* Décrit le résultat voulu sans décrire la manière de l'obtenir
* Les SGBDR déterminent automatiquement la manière optimale d'effectuer
les opérations nécessaires à l'obtention du résultat

## Extraction des données d'une table

```sql
SELECT noms_colonnes_séparés_par_virgules
    FROM nom_table;
```

Sélectionne toutes les lignes d'une table
\   

### Précisions sur les colonnes affichées

* `*` pour toutes les colonnes

```sql
SELECT *
    FROM nom_table;
```
\   

* `DISTINCT` pour sélectionner une seule occurrence de chaque valeur des colonnes en question

```sql
SELECT DISTINCT noms_colonnes_séparés_par_virgules
    FROM nom_table;
```

Exemple :

```sql
SELECT DISTINCT categorie, genre
    FROM evolution;
```

## Extraction des données d'une table

```sql
SELECT noms_colonnes_séparés_par_virgules
    FROM nom_table
    WHERE nom_colonne op_comp valeur op_bool nom_colonne op_comp valeur;
```

Sélectionne uniquement les lignes qui respectent la clause du `WHERE`
\   

### La clause porte sur les valeurs des colonnes

* Utilisation d'opérateurs de comparaison (op_comp) : `=`, `<>`, `!=`, `>`, `>=`, `<`, `<=`
* Utilisation d'opérateurs booléens (op_bool) : `AND`, `OR`
  * `AND` : combinaisons de conditions sur des colonnes différentes
  * `OR` : plusieurs valeurs possibles pour une même colonne

Exemples :

```sql
SELECT code, effectif
	FROM evolution
	WHERE categorie="Agriculteurs Exploitants" AND genre="Femmes";

SELECT code, categorie, effectif
	FROM evolution
	WHERE categorie="Agriculteurs Exploitants" OR categorie="Ouvriers";
```

## Extraction des données d'une table

```sql
SELECT abrev.nom_colonne AS nom_affiché
    FROM nom_table AS abrev
    ORDER BY nom_colonne [DESC];
```

Change l'affichage et le nommage des données

### `AS`

* Associé à un nom de colonne : change le nom affiché de la colonne
dans le résultat

* Associé à un nom de table : permet d'abrévier le nom de la table pour
préciser de quelle table provient une colonne dont le nom est utilisé
par plusieurs tables. Cette abréviation **doit** être utilisée dans le
reste de la requête.

### `ORDER BY`

* Trie les données selon la colonne précisée
* Par défaut, le tri est dans l'ordre croissant, `DESC` permet d'obtenir
l'ordre décroissant

## Fonctions de calcul sur les données extraites

```sql
SELECT FONCTION(nom_colonne)
    FROM nom_table;
```

Applique une fonction sur les valeurs d'une colonne

* `COUNT` : compte le nombre de lignes sélectionnées

* `MIN`, `MAX` : renvoie la valeur minimum ou maximum de la colonne,
parmi les lignes sélectionnées

* `SUM`, `AVG` : calcule la somme ou la moyenne des valeurs
**numériques** de la colonne, parmi les lignes sélectionnées

<!-- new slide --> 
##

Exemple :

```sql
SELECT AVG(effectif) AS Moy_employes
	FROM evolution
	WHERE categorie="Employés";
```

* Pas au programme, `GROUP BY` : agrège ensemble les valeurs identiques
d'une colonne pour appliquer une fonction à chacun des sous-ensembles

Exemple :

```sql
SELECT code, AVG(effectif) AS Moy_employes
	FROM evolution
	WHERE categorie="Employés"
    GROUP BY code;
```

Calcule la moyenne des effectifs des `Employés` (`Hommes` et `Femmes`)
pour chaque ville 

## Exemples avec ou sans agrégat

![résultats avg sans GROUP BY](fig/avg.png "sans agrégat") ![résultats avg avec GROUP BY](fig/avg_group_by.png "avec agrégat")

## Extraction des données de deux tables

### Produit cartésien
* Comme son nom l'indique, génère de façon exhaustive toutes les
associations possibles entre les lignes des deux tables
  * Nb_total_lignes = Nb_lignes_ville * Nb_lignes_evolution = 650 * 10400
* Non pertinent

![](fig/produit_cartesien.png)

## Extraction des données de deux tables

### `JOIN ON` 
* Génère uniquement les associations entre les lignes qui sont liées
par des clés primaires et étrangères identiques
  * Nb_total_lignes = Nb_lignes_table_clé_étrangère = NB_lignes_evolution
* À utiliser pour associer deux tables

![](fig/join_on.png)

## Modification des données

### Syntaxe

* `INSERT` : ajoute une nouvelle ligne de données dans une table

```sql
INSERT INTO nom_table VALUES (liste_valeurs_dans_ordre_colonnes_table);
INSERT INTO nom_table (liste_nom_colonnes_à_remplir)
    VALUES (liste_des_valeurs_à_insérer_dans_ordre_liste_colonnes);
```
  
\  
  
* `UPDATE` : met à jour la ou les lignes qui respectent la clause du `WHERE`

```sql
UPDATE nom_table SET nom_colonne1=valeur1, nom_colonne2=valeur2
    WHERE nom_colonne op_comp valeur op_bool nom_colonne op_comp valeur;
```
  
\  
  

* `DELETE` : efface la ou les lignes d'une table qui respectent la clause du `WHERE`

```sql
DELETE FROM nom_table WHERE nom_colonne op_comp valeur op_bool nom_colonne op_comp valeur;
```

## Modification des données

### Respect de l'intégrité des données

* Une clé primaire doit être unique et non NULL
  * On ne peut pas insérer une ligne avec une clé primaire qui existe déjà
  * On ne peut pas modifier la valeur d'une clé primaire en une autre valeur
  qui existe déjà
* Une clé étrangère doit référencer une clé primaire existante
  * Il faut créer la ligne contenant la clé primaire avant une ligne
  contenant une clé étrangère la référençant
  * On ne peut pas modifier une clé primaire si elle est déjà référencée
  * On ne peut pas effacer une ligne contenant une clé primaire déjà référencée
* Il est possible de mettre des contraintes sur les clés pour gérer
les cascades de modifications (interdiction ou gestion automatique)

## Important

* Sources: articles Wikipedia

  * [Base de données relationnelle](https://fr.wikipedia.org/wiki/Base_de_donn%C3%A9es_relationnelle)
  * [Clé primaire](https://fr.wikipedia.org/wiki/Cl%C3%A9_primaire)
  * [Clé étrangère](https://fr.wikipedia.org/wiki/Cl%C3%A9_%C3%A9trang%C3%A8re)
  * [Modèle relationnel](https://fr.wikipedia.org/wiki/Mod%C3%A8le_relationnel)
  * [Structured_Query_Language](https://fr.wikipedia.org/wiki/Structured_Query_Language)

* Remerciements

  * Patricia pour la co-rédaction du sujet de TP
  * Eric et Philippe pour les relectures et corrections
