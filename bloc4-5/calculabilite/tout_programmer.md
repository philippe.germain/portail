---
title: Calculabilité - Décidabilité
subtitle: Peut-on tout programmer ?
author: Équipe pédagogique DIU EIL. Univ. Lille
thanks: Ce document est mis à disposition selon les termes de la [Licence Creative Commons (CC-BY-3.0 FR)](https://creativecommons.org/licenses/by/3.0/fr/) \protect ![](images/cc-by.png){width=8%}
date: décembre 2020
---
## Références au programme de NSI classe de Terminale (1/2)


1. dans la partie « Histoire de l'informatique » 

   ![](images/pgmme_nsi_tale_histoire.png){width=65%}

## Références au programmes de NSI classe de Terminale (2/2)


2. dans la partie « Langages et programmation » :
  
   ![](images/pgmme_nsi_tale_lang_prog.png){width=65%}


# Peut-on tout programmer ? Réponse 1


## Combien de programmes ?

 
. . .

* Une infinité bien sûr, mais quelle infinité ?

* On ordonne les programmes par taille croissante, et pour une taille donnée on les ordonne par ordre lexicographique.

* Cet ordre permet de numéroter les programmes par tous les entiers naturels :
  $$ P_0, P_1, P_2, \ldots $$

* L'infinité des programmes est donc égale celle des entiers : $\mathrm{card}(\mathbb{P}) = \mathrm{card}(\mathbb{N}) = \aleph_0$.

## Combien de fonctions ?


. . .

* Une infinité bien sûr, mais quelle infinité ?

* Considérons uniquement les fonctions à un paramètre entier ne renvoyant que les valeurs 0 ou 1 (prédicats unaires).

* Si $A$ est un sous-ensemble de $\mathbb{N}$, on note $1_A(n)$ la fonction qui vaut 1 si $n\in A$ et 0 sinon.


* Considérons la fonction
  $$\begin{array}{rccl}
  {\Phi~:}&{\mathcal{P}(\mathbb{N})}&\rightarrow&{\mathcal{F}_{0,1}}\\
  &{A}&\mapsto&{1_A}.
  \end{array}$$

* C'est une bijection entre l'ensemble des parties de $\mathbb{N}$ et l'ensemble des prédicats unaires sur $\mathbb{N}$.

* $\mathrm{card}(\mathcal{F}_{0,1}) = \mathrm{card}(\mathcal{P}(\mathbb{N})  (= \mathrm{card}(\mathbb{R}))$. 

## Le nombre de parties dépasse toujours le nombre d'éléments


* Théorème dû à G. Cantor (fin XIXème siècle) :

	Pour tout ensemble $E$ on a
	
	$$\mathrm{card}(E) < \mathrm{card}(\mathcal{P}(E)).$$
	
## Conclusion


. . .

* $\mathrm{card}(\mathbb{P}) < \mathrm{card}(\mathcal{F}_{0,1}).$

* Il y a beaucoup plus de prédicats unaires sur les entiers que de programmes.

* Les fonctions non programmables sont (infiniment) plus nombreuses que celles qui le sont.
# Peut-on tout programmer ? Réponse 2


## Une fonction


*
  ``` {.sourceCode .python}
def collatz(n):
    if n <= 1:
	   return 1
	elif n % 2 == 0:
	   return collatz(n // 2)
	else:
	   return collatz(3*n + 1)
```
	   
* s'arrête-t-elle pour tout entier $n\in\mathbb{N}$ ?

* pas de réponse connue à ce jour.

## Le problème de l'arrêt


* $$ \mathtt{arret\_garanti(f, n)} = \left\{\begin{array}{ll} \mathtt{True} & \mbox{si le calcul de \texttt{f(n)} s'arrête}\\
                                                   \mathtt{False} & \mbox{sinon}
                                 \end{array}\right.
  $$




## Une autre fonction


* supposons la fonction `arret_garanti` programmée

*
  ``` {.sourceCode .python}
def une_fonction(n):
    if arret_garanti(une_fonction, n):
       return une_fonction(n)
    else:
       return 1
```

* Que vaut `une_fonction(1)` ?

* le calcul de `une_fonction(1)` s'arrête si et seulement si il ne s'arrête pas !

## Conclusion


* `arret_garanti` n'est pas programmable !
D'autres fonctions non programmables
====================================


Théorème de Rice
----------------


* Théorème dû à H. G. Rice (1951) :

	Toute propriété \emph{non triviale} des programmes est indécidable.

* arrêt des programmes
* conformité d'un programme à sa spécification (preuve)
* égalité de deux programmes
* et bien d'autres ...

* Grande déception chez les enseignants d'informatique rêvant d'un correcteur automatique de projets d'étudiants.

Pavage du plan avec des tuiles de Wang
--------------------------------------

* Les tuiles de Wang

  ![](images/wang7.pdf){width=65%}
  

* assembler plusieurs tuiles

  ![](images/wang2.pdf){width=35%}
  
* rotations interdites

  ![](images/wang3.pdf){width=55%}

Paver le plan (2/4)
-----------------

Est-il possible de paver le plan infini avec un jeu de tuiles donné ?

* Exemple 1

  ![](images/wang5.pdf){width=30%}
  
* Solution

  ![](images/wang6.pdf){width=35%}
  
* Conclusion : impossible de paver le plan avec ces tuiles


Paver le plan (3/4)
-----------------



* Exemple 2

  ![](images/wang7.pdf){width=75%}
  
* Solution

  ![](images/wang8.pdf){width=40%}
  
* Conclusion : possible de paver le plan avec ces tuiles.


Paver le plan (4/4)
-------------------

* Le problème du pavage : décider si, étant donné un jeu de tuiles, il est possible ou non de paver le plan.

* Réponse : ce problème est indécidable (R. Berger, 1966).

# Trombinoscope de la calculabilité 


## ![](images/alonzo-church.jpg){width="100px"} 


. . .

* Alonzo Church (1903-1995)
* $\lambda$-calcul (\emph{An unsolvable problem of elementary number theory}, 1936).

  $$ \lambda f.(\lambda x.(f\ (x\ x))\ \lambda x.(f\ (x\ x))).$$
* Formalisme encore très largement utilisé en informatique (programmation fonctionnelle, théorie des types).

## ![](images/stephen-kleene.png){width="100px"} 


. . .

* Stephen Cole Kleene (1909-1994)
* Fonctions récursives (\emph{General recursive functions of natural numbers}, 1936).

## ![](images/alan-turing.jpg){width="100px"} 


. . .

* Alan Mathison Turing (1912-1954)
* Machines de Turing (\emph{On computable numbers, with an application to the Entscheidungsproblem}, 1936).

  ![](images/Turing_machine_2b.svg.png){width=50%}

* Modèle théorique de machine très utilisé en informatique (mesure de complexité des problèmes).


## Thèse de Church 


. . .

* Tous ces formalismes sont équivalents.
* Calculable = exprimable dans l'un de ces formalismes.
# Bibliographie

## Quelques livres/articles en anglais


* *The New Turing Omnibus, 66 excursions in computer science*, A.K. Dewdney, Holt, 2001, \texttt{ISBN-13 978-0805071665}.
* *Computers Ltd: What they really can't do*, D. Harel, OUP Oxford, 2003, \texttt{ISBN-13 978-0198604426}. (\small La première partie de ce livre est consacrée à la non calculabilité/ non décidabilité. La seconde partie est consacrée aux problèmes calculables ou décidables mais de complexité algorithmique telle que leur traitement effectif en machine est inacessible.)
	
	
## Quelques livres/articles en français

* *La machine de Turing*, A. Turing, J.Y. Girard, Points Science n° S131, 1995, \texttt{ISBN-13 978-2020369282}.  (\small Ce livre en collection de poche contient une traduction en français de l'article qu'Alan Turing a publié en 1936 et dans lequel il présente ses machines.)
	
* *Introduction à la calculabilité*, P. Wolper, Dunod 2006, \texttt{ISBN-13 978-2100499816}. (\small livre universitaire)
	
## Pour NSI

* *Une preuve pour le lycée de l'indécidabilité du problème de l'arrêt*, M. Journault, P. Lafourcade, R. Poulain, M. More, colloque [DidaPro8](https://www.didapro.org/8/programme/#article), Lille, février 2020. (\small  Tout est dans le titre. Et c'est accompagné d'un matériel qu'on peut utiliser en classe et en débranché pour appréhender l'indécidabilité de l'arrêt. Le tout est disponible sur le site de l'[IREM de Clermont-Ferrand](http://www.irem.univ-bpclermont.fr/Indecidabilite-du-probleme-de-l).)

* *Spécialité Numérique et sciences informatiques : 24 leçons avec exercices corrigés - Terminale*, T. Balabonski, S. Conchon, J.-C. Filliâtre, K. Nguyen, Ellipses 2020, \texttt{ISBN-13 978-2340038554}.
  (\small Le chapitre 16 est consacré à la calculabilité et montre l'existence de problème non calculable en traitant celui de la détection d'une éventuelle division par zéro.)
