;;; ------------------------------
;;; 1er exo 
;;; ------------------------------------------------------------

;;; en "binaire-décimal"
00:	399	
01:	011	
02:	112	
03:	301	
04:	608	
05:	402	
06:	299	
07:	599	
08:	412	
09:	299	
10:	599	
11:	123	
12:	042	

;;; version mnémonique 

00:	NOP			; on passe 
01:	LDA 11			; contenu de le mot mémoire 11 dans A 
02:	LDB 12			; contenu de le mot mémoire 12 dans B
03:	SUB			; R reçoit A-B
04:	JPP 08			; saut en 08 si R>0, ie si A>B
05:	MOV 02			; contenu de A dans R
06:	STR 99			; contenu de R dans 99, ie affiche A
07:	HLT			; fin d'exécution
08:	MOV 12			; contenu de B dans R 
09:	STR 99			; contenu de R dans 99, ie affiche B
10:	HLT			; fin d'exécution
11:	123			; donnée utilisée dans le programme
12:	42			; donnée utilisée dans le programme

;;; ce programme affiche 42 (surprise !),
;;; plus précisément, le min des valeurs des cases 11 et 12 

;;; ------------------------------
;;; 2e exo 
;;; ------------------------------------------------------------

;;;  version binaire 

00:	514
...
14:	099
15:	402
16:	211
17:	099
18:	402
19:	212
20:	501

;;; version mnémonique

00:	JMP 14
;;; 01: à 12: identique au précédent exo
;;; 13: une valeur quelconque 
14:	LDA 99			; saisie clavier dans A
15:	MOV A R			; contenu de A dans R
16:	STR 11			; contenu de R, copie de la saisie, dans 11
17:	LDA 99			; 2e saisie clavier
18:	MOV A R
19:	STR 12			; copie de la saisie dans 12
20:	JMP 01			; code précédent

;;; ce programme saisit deux valeurs, les écrit dans les mots mémoire
;;; 11 et 12, et appelle le code précédent qui va afficher le min des
;;; deux valeurs. 

;;; ------------------------------
;;; minimum de 2 valeurs saisies au clavier 
;;; ------------------------------------------------------------

00:	LDA 99			; 1re valeur
01:	LDB 99			; 2e valeur
02:	SUB			; A-B dans R
	;; mettre la valeur à afficher dans R
03:	JPP 06			; saut si R>0, ie A>B
	;; afficher B, donc B dans R
04:	MOV B R
05:	JMP
	;; afficher A, donc A dans R
06:	MOV A R
	;; afficher R
07:	STR 99
08:	HLT

;;; ------------------------------
;;; produit de 2 entiers  
;;; ------------------------------------------------------------

;;; version pseudo langage de haut niveau 
	;; x
	;; y (supposé positif)
	;; somme
	somme = 0
début:	if y>0 goto vrai:
	goto fin:
vrai:	somme += x
	y -= 1
	goto début:	
fin:

;;; 1re version mnémonique
	
;;; on utilise @x, @y, @somme @0, et @1 pour noter les adresses des
;;; variables et constantes en mémoire
;;; on utilise des labels pour les numéros d'instructions
	
	;; somme = 0 
	LDA @0
	MOV A R
	STR @somme
	;; if y>0
début:	LDA @y
	MOV A R
	JPP vrai:
	JMP fin:
vrai:	;; somme += x
	LDA @somme	
	LDB @x
	ADD
	STR @somme
	;;  y -= 1
	LDA @y
	LDB @1
	SUB
	STR @y
	JMP début:
fin:	

;;; 2e version mnémonique

;;; on numérote les instructions
;;; à partir de 1 pour laisser le choix du boot sur autre chose
	

01:	LDA @0
	MOV A R
	STR @somme
04:	LDA @y
	MOV A R
	JPP 08
	JMP 17
08:	LDA @somme	
	LDB @x
	ADD
	STR @somme
	LDA @y
	LDB @1
	SUB
	STR @y
	JMP 04
17:

;;; 3e version mnémonique

;;; on alloue les variables, par exemple à la suite du code
;;; x en 18, y en 19, somme en 20
;;; on ne traite pas des constantes 0 et 1 
01:	LDA @0
	MOV 02
	STR 20
04:	LDA 19
	MOV 02
	JPP 08
	JMP 17
08:	LDA 20	
	LDB 18
	ADD
	STR 20
	LDA 19
	LDB @1
	SUB
	STR 19
	JMP 04
17:	NOP			; un peu de marge ;)
18:	<x>
19:     <y>
20:     <somme>

;;; 4e version, on peut enfin traduire en binaire...

01:	0xx
02:	402
	;; etc.
21:	

;;; ------------------------------
;;; produit de 2 entiers - version registres 
;;; ------------------------------------------------------------

;;; version pseudo langage de haut niveau 
	;; x
	;; y (supposé positif)
	;; somme
	somme = 0
début:	if y>0 goto vrai:
	goto fin:
vrai:	somme += x
	y -= 1
	goto début:	
fin:

;;; on utilise les registes R3 pour somme, R4 pour x, R5  pour y 
	;; somme = 0
	MOV 0 R3 		; mise à 0 de R3 - codée MOV 6 3, ie 463
	LDA @x
	MOV A R4
	LDA @y
	MOV A R5
debut:	;; y>0
	MOV R5 R
	JPP vrai:
	JMP fin:
vrai:
	;; somme += x
	MOV R3 A
	MOV 1 B
	ADD
	MOV R R3
	;; y = y -1
	MOV R5 A
	MOV 1 B
	SUB
	MOV R R5
	JMP debut:
fin:	MOV R3 R
	STR @somme
	
;;; avec seulement deux registres R3 et R4 - pour 3 variables
;;; on choisit de garder somme et y en registre - qui sont lus et écrits
;;; et de faire les accès mémoire pour x - qui n'est que lu, moins souvent
;;; accédé.


;;; ------------------------------
;;; carré d'un entier - version appel de fonctions
;;; ------------------------------------------------------------

;;; On dispose des instructions d'appel de fonction, CAL et RET
;;; (mais pas encore des instructions de mnipulation directe de la pile)

;;; On calcule le carré d'un entier à partir du code de calcul du produit

;;; Soit la fonction mul-v1 de calcul du produit de 2 entiers.
14:	<x, 1re opérande>
15:	<y, 2e opérande>
16:	<résultat, produit dee x et y>
17:	LDA 14			; recupère le 1er paramètre
18:	LDB 15			; et le second
19:	...
	...
36:	STR 16 			; résultat à l'adresse dédiée
				; (nous pourrions utliser le registre R)
37:	RET

;;; Une fonction carré-v1 qui utilise mul-v1
38:	<x, opérande>
39:	<résultat>
40:	LDA 38			; recupère opérande
	MOV A R			; operande dans R
	STR 14			; 1er opérande mul-v1
	STR 15			; 2de opérande mul-v1
	CAL 17			; appel de mul-v1
	LDA 16			; recupère le résultat
	MOV A R 		; résultat dans R
47:	STR 39			; résultat à l'adresse dédiée

;;; calcul du cube d'une valeur en utilisant mul-v1 et carre-v1
48:	
