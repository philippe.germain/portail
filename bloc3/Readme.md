Bloc 3 - Architectures matérielles et robotique, systèmes et réseaux
====================================================================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

**Intervenant·es** —
Philippe Marquet,
Thomas Vantroy,
Frédéric Guyomarc'h,
Benoit Papegay,
Lauent Noé,
Patric Thibaud.

Prise en main de l'environnement
================================

jeudi 7 et vendredi 8 janvier 2021

* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - Très et trop brève introduction
  - Découverte de l'interpréteur de commandes
* [GitLab](seance0/gitlab.md)
  - Découverte de l'environnement GitLab
* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - Utiliser l'interpréteur de commandes
  - ...

Architecture des machines informatiques et des systèmes d'exploitation
======================================================================

vendredi 8 janvier 2021

conférence de Gilles Grimaud, « _Turing, von Neumann, ... architecture
des machines informatiques et des systèmes d'exploitation_ ».

* Machine de Turing
  [page Wikipedia fr](https://fr.wikipedia.org/wiki/Machine_de_Turing)
* article fondateur d'Alan Turing, 1936 _« On Computable Numbers, with
  an Application to the Entscheidungsproblem »_
 - notes 10 et 11 de la page Wikipedia [Alan Turing](https://fr.wikipedia.org/wiki/Alan_Turing#cite_note-on_computable_numbers-10) 
* _Le modèle d’architecture de von Neumann_ par Sacha Krakowiak, sur
  [interstices.info/](https://interstices.info/le-modele-darchitecture-de-von-neumann/)

Système d'exploitation
======================

vendredi 8 janvier 2021

présentation par Philippe Marquet « _Système d'exploitation — système
de fichiers, processus, shell_ ». 
	
* support de présentation
	[4 pages par page](psfssh/psfssh-4up.pdf) /
	[1 page par page](psfssh/psfssh-slide.pdf) /
	[source Markdown](psfssh/psfssh.md)

Architecture von Neumann
========================

lundi 13 décembre 2021 \
mardi 14 décembre 2021

Architecture von Neumann

* [M999 - activité d'informatique débranchée](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/blob/master/bloc3/archi/Readme.md#m999-le-processeur-d%C3%A9branch%C3%A9)

Architecture des ordinateurs
============================

mercredi 15 décembre 2021

* [support de présentation](archi/Readme.md#architecture-des-ordinateurs)


Système d'exploitation et logiciels libres
==========================================
  
mercredi 15 décembre 2021
  
Informatique libre

* support de présentation
  [4 pages par page](libre/libertes-papier.pdf) / 
  [1 page par page](libre/libertes-ecran.pdf)

<!-- eof -->
