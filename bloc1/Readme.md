Bloc 1 - Représentation des données et programmation
====================================================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

**Intervenant·es** —
Jean-Christophe Routier,
Philippe Marquet,
Benoit Papegay,
Patricia Everaere,
Sylvain Salvati,
Francesco de Comité,
Laetitia Jourdan,
Maude Pupin,
Frédéric Guyomarc'h,
Pierre Allegraud,
Laurent Noé.

Premiers pas avec Thonny et Python
==================================

jeudi 7 janvier 2021

* [Prise en main de Thonny](thonny/readme.md)
* [Traitement de séquences ADN](adn/readme.md)
* [Jeu de la vie](jeu-de-la-vie/readme.md)

Bonnes pratiques de programmation Python
========================================

mardi 12 janvier 2021

* [Bonnes pratiques de programmation Python](bonnes_pratiques/readme.md)
  - « amphi »
  - doctest, mode pas à pas
  - modules 

Seconds pas avec Python
=======================

mardi 12 janvier 2021

* [Jeu de la vie](jeu-de-la-vie/readme.md) à terminer
* [Générateur de phrases de passe](phrases_de_passe/readme.md)

> À propos de la [recherche des voisins dans le jeu de la vie](jeu-de-la-vie/jeu-vie-voisins.md)

Récursivité
===========

mercredi 13 janvier 2021\
jeudi 14 janvier 2021

* [notes de cours](recursivite/readme.md)
* support de TD et TP
	[version PDF](recursivite/exo-recursivite.pdf)
	/ [version en ligne](recursivite/exo-recursivite.md)
	<!--
	une correction possible pour ces exercices
	[solutions-exo-recursivite.py](recursivite/src/solution-exo-recursivite.py) --> 
* support d'activité sans ordinnateur
  [la pile débranchée](pile-debranchee/Readme.md) 

Travail préparatoire
====================

pour le 30 juin 2021

* [travail préparatoire](prepaChicon/readme.md)

Modules, dictionnaires et exceptions
====================================

mercredi 30 juin 2021

* Modules
  - [support de présentation](modules/readme.md)
  - fichiers sources de l'exemple des cartes à jouer
	[src/](modules/src/)
* Dictionnaires et exceptions
  - [Notebook Jupyter de présentation](dictionnaires/readme.md)

Course du chicon
================

mercredi 30 juin 2021\
jeudi 1er ou vendredi 2 juillet 2021

* [support de TP](bloc1/course_chicon/readme.md)

Wator
=====

vendredi 2 ou lundi 5 juillet 2021\
mardi 6 juillet 2021

Simulation proie-prédateur

* [support de TP](wator/readme.md)

 - [Une correction possible](wator/src) qui utilise les dictionnaires pour modéliser les poissons. \
    Usage via la fonction `run_wator` de `watorMain.py`, déclenchée à l'exécution du script avec les valeurs `run_wator(25*25*200,25,25)`.
   ```python
   def run_wator(nb_steps, width=30, height=30, tuna_percent = 30, shark_percent = 10):
    '''
    run wator simulation for nb_steps steps
    sea is width x height and at beginning there is ~tuna_percent% tuna and ~shark_percent% shark
    @param {int} nb_steps nb evolution steps of simulation
    @param {int} width width of sea (default is 30)
    @param {int} height height of sea (default is 30)
    @param {int} tuna_percent tuna initial percentage
    @param {int} shark_percent shark initial percentage
    @return list of tuples of the form (step, nb_tunas, nb_sharks) for each step of simulation    
    '''
   ```

Bonnes pratiques de programmation 2
===================================

mardi 6 juillet 2021

* [support de présentation](bonnes_pratiques_2/readme.md) 
* [exemples_codes.py](bonnes_pratiques_2/exemples_codes.py) 

Représentation des données
==========================

lundi 13 décembre 2021 \
mardi 14 décembre 2021

[Représentation des données](repres_donnees/Readme.md)

* support de présentation
* resssources complémentaires
* travaux pratiques Idéogrammes de Bob Morane
* travaux pratiques Base 64

Interface de module – Jeux à deux joueurs, minmax
=================================================

mardi 14 décembre 2021 \
mercredi 15 décembre 2021 

Jeux à deux joueurs, algorithme minmax

* [support de présentation](minmax/algo-minMax.odp)

Notion d'interface de module

* [présentation rapide](minmax/readme.md#notion-dinterface-de-module-rapidement)

Travaux pratiques

* [Introduction, objectif](minmax/readme.md)
* [Les jeux à deux joueurs](minmax/sujet/jeux_deux_joueurs.md)

<!-- eof -->
